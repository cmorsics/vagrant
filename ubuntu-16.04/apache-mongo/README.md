# vagrant

# Description
Vagrant setup including:
* Ubuntu 16.04 LTS
* Apache 2
* MongoDB

# Install VirtualBox
https://www.virtualbox.org/wiki/Downloads

# Install Vagrant
https://www.vagrantup.com/downloads.html

# Start VM
vagrant up

# Destroy VM
vagrant destroy

# Change MongoDB Bind URL
/etc/mongodb.conf bind_ip to 0.0.0.0