# vagrant

# Description
Vagrant setup including:
* Ubuntu 16.04 LTS
* Java JDK 8
* ElasticSearch 5.x 
* Kibana 5.x
* Apache 2

# Install VirtualBox
https://www.virtualbox.org/wiki/Downloads

# Install Vagrant
https://www.vagrantup.com/downloads.html

# Start VM
vagrant up

# Destroy VM
vagrant destroy