# vagrant

# Description

Collection of vagrant files

# Install VirtualBox
https://www.virtualbox.org/wiki/Downloads

# Install Vagrant
https://www.vagrantup.com/downloads.html

# Start VM
vagrant up

# Destroy VM
vagrant destroy
